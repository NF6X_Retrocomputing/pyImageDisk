#!/usr/bin/env python3

"""Convert raw image of DEC Rainbow disk into ImageDisk format."""

import argparse
import imd

_cylinders = 80
_sectors   = 10
_secstart  = 1
_secsize   = 512
_imgsize   = _cylinders * _sectors * _secsize

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('-c', '--commentfile', default=None,
                        help='Name of file containing comment to be included' \
                        ' in the ImageDisk file. It can be free-form text.')

    parser.add_argument('infile', metavar='INFILE',
                        help='Raw image of DEC Rainbow disk to be converted.' \
                        ' Image file must be exactly {:d} bytes long.' \
                        .format(_imgsize))

    parser.add_argument('outfile', metavar='OUTFILE', nargs='?',
                        help='Output file to be created. If not specified,' \
                        ' output file will be input file with .imd appended.')

    args = parser.parse_args()

    # Read in the raw image file and check it for expected length
    fp = open(args.infile, 'rb')
    imgdata = fp.read()
    fp.close()
    if len(imgdata) != _imgsize:
        raise ValueError('Bad image file:' \
                         ' Expected {:d} bytes but read {:d}' \
                         .format(_imgsize, len(imgdata)))

    # Make an empty ImageDisk image
    rainbowdisk = imd.disk()
    imgoffset = 0

    # Append our new comment to the existing one, which already
    # includes the ImageDisk file magic number. We don't want to
    # lose that magic number.
    if args.commentfile is None:
        rainbowdisk.comment = rainbowdisk.comment + '\nDEC Rainbow Disk Image'
    else:
        cf = open(args.commentfile, 'rb')
        ctext = cf.read()
        cf.close()
        # Comment must not contain 0x1A
        rainbowdisk.comment = rainbowdisk.comment + '\n' \
            + bytes([c for c in ctext if c != 0x1A]).decode('ascii')

    for c in range(_cylinders):
        # Create a new cylinder instance. This will be a list of tracks,
        # and since this is a single-sided format, cyl[0] will be the
        # single track for head 0 on this cylinder. We will create each
        # cylinder without interleave or skew, so that physical order
        # is the same as logical order. That makes the next step easier.
        cyl = imd.cylinder(heads=1,
                           sectors=_sectors,
                           mode=imd.MODE_250K_MFM,
                           start=_secstart,
                           skip=0,
                           skew=0,
                           size=_secsize,
                           fill=0xFF,
                           cyl_num=c)

        # Replace contents of each sector with data from raw image.
        # Pre-initialized sectors do not have any skip or skew, so
        # they are already in logical order.
        for sector in cyl[0]:
            sector[:_secsize] = imgdata[imgoffset:imgoffset+_secsize]
            imgoffset = imgoffset + _secsize

        # If we wanted to apply interleave or skew to the physical
        # track, we could do it here. But it is not needed for the
        # Rainbow format.

        # Append the cylinder to the ImageDisk image
        rainbowdisk.append(cyl)

    # Write out the .imd file. Compressible sectors (that is, sectors
    # where all bytes have the same value) will be compressed automatically.
    if args.outfile is None:
        rainbowdisk.save('{:s}.imd'.format(args.infile))
    else:
        rainbowdisk.save(args.outfile)
