#!/usr/bin/env python3

"""For each .IMD file, print number of sides and filename."""

import argparse
import imd

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('imdfile', nargs='+')
    args = parser.parse_args()

    for f in args.imdfile:
        try:
            # Load .IMD file
            d = imd.disk(filename=f)

            # Determine maximum number of heads found on all cylinders.
            h = 1
            for c in d:
                # For each cylinder, see if it has more heads than
                # we have encountered before.
                h = max(h, len(c))

            # Print number of sides and filename.
            print('{:d} {:s}'.format(h, f))
        except:
            # Probably not a .IMD file.
            print('? {:s}'.format(f))