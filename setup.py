#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Installation script for pyImageDisk.

pyImageDisk provides support for creating and manipulating ImageDisk
(.IMD) files.

Usage examples:

    Install in your user directory:
        ./setup.py install --user

    Install in the system default location:
        sudo ./setup.py install

    Install under /usr/local:
        sudo ./setup.py install --prefix /usr/local

    In some cases, it may be necessary to set PYTHONPATH
    to point to the installation location, e.g.:
        sudo PYTHONPATH=/usr/local/lib/python3.7/site-packages \
        ./setup.py install --prefix /usr/local
"""


from setuptools import setup
from imd import __version__, __pkg_url__, __dl_url__

long_description="""pyImageDisk includes a package and utilities for
examining, creating and manipulating ImageDisk (*.imd) files. These
files are useful containers for archival of floppy disk formats created
with common FM and MFM disk controller chips as used in many vintage
computer systems. The original DOS utility for creation of ImageDisk
files from real floppy disks, and creation of real floppy disks from
ImageDisk files was written by Dave Dunfield, and may presently be found
here: http://www.classiccmp.org/dunfield/img/index.htm """


setup(name                 = 'pyImageDisk',
      version              = __version__,
      description          = 'Tools for manipulating ImageDisk (*.imd) files.',
      long_description     = long_description,
      author               = 'Mark J. Blair',
      author_email         = 'nf6x@nf6x.net',
      url                  = __pkg_url__,
      download_url         = __dl_url__,
      license              = 'GPLv3',
      packages             = ['imd'],
      scripts              = ['imdutil'],
      test_suite           = 'imd.test_all.test_all',
      include_package_data = True,
      keywords             = 'ImageDisk IMD',
      classifiers          = ['Development Status :: 4 - Beta',
                              'Environment :: Console',
                              'Intended Audience :: Developers'
                              'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
                              'Natural Language :: English',
                              'Operating System :: OS Independent',
                              'Programming Language :: Python',
                              'Programming Language :: Python :: 3',
                              'Programming Language :: Python :: 3.7',
                              'Topic :: System :: Archiving',
                              'Topic :: System :: Emulators',
                              'Topic :: System :: Filesystems',
                              'Topic :: System :: Software Distribution',
                              'Topic :: Utilities'])
