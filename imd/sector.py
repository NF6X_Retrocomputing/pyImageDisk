#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016-2019 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""This package defines sector objects stored by the track class."""

#from . import track
from . import util
import string
__printable__ = string.ascii_letters + string.digits + string.punctuation


# Sector record types
SEC_REC_NONE          = 0x00 # Sector unreadable. No data follows.
SEC_REC_NORM          = 0x01 # Normal data. (sector size) bytes follow.
SEC_REC_COMP          = 0x02 # Compressed: One byte follows.
SEC_REC_NORM_DDAM     = 0x03 # Normal with deleted data address mark (DDAM).
SEC_REC_COMP_DDAM     = 0x04 # Compressed with DDAM.
SEC_REC_NORM_ERR      = 0x05 # Normal; error occurred while reading.
SEC_REC_COMP_ERR      = 0x06 # Compressed; error occurred while reading.
SEC_REC_NORM_DDAM_ERR = 0x07 # Normal with DDAM; error occurred while reading.
SEC_REC_COMP_DDAM_ERR = 0x08 # Compressed with DDAM; error while reading.

SEC_REC_TYPE_DESC  =['Sector unavailable/unreadable',
                     'Normal sector data',
                     'Compressed sector data',
                     'Normal sector data with DDAM',
                     'Compressed sector data with DDAM',
                     'Normal sector data with read error',
                     'Compressed sector data with read error',
                     'Normal sector data with DDAM and read error',
                     'Compressed sector data with DDAM and read error']

SEC_REC_ALL_TYPES  = [SEC_REC_NONE,
                      SEC_REC_NORM,
                      SEC_REC_COMP,
                      SEC_REC_NORM_DDAM,
                      SEC_REC_COMP_DDAM,
                      SEC_REC_NORM_ERR,
                      SEC_REC_COMP_ERR,
                      SEC_REC_NORM_DDAM_ERR,
                      SEC_REC_COMP_DDAM_ERR]

SEC_REC_NORM_TYPES = [SEC_REC_NORM,
                      SEC_REC_NORM_DDAM,
                      SEC_REC_NORM_ERR,
                      SEC_REC_NORM_DDAM_ERR]

SEC_REC_COMP_TYPES = [SEC_REC_COMP,
                      SEC_REC_COMP_DDAM,
                      SEC_REC_COMP_ERR,
                      SEC_REC_COMP_DDAM_ERR]

SEC_REC_ERR_TYPES  = [SEC_REC_NORM_ERR,
                      SEC_REC_COMP_ERR,
                      SEC_REC_NORM_DDAM_ERR,
                      SEC_REC_COMP_DDAM_ERR]

SEC_REC_DDAM_TYPES = [SEC_REC_NORM_DDAM,
                      SEC_REC_COMP_DDAM,
                      SEC_REC_NORM_DDAM_ERR,
                      SEC_REC_COMP_DDAM_ERR]

# Map type to equivalent type with compression on
SEC_REC_COMPRESS_ON = {
    SEC_REC_NONE:          SEC_REC_NONE,
    SEC_REC_NORM:          SEC_REC_COMP,
    SEC_REC_COMP:          SEC_REC_COMP,
    SEC_REC_NORM_DDAM:     SEC_REC_COMP_DDAM,
    SEC_REC_COMP_DDAM:     SEC_REC_COMP_DDAM,
    SEC_REC_NORM_ERR:      SEC_REC_COMP_ERR,
    SEC_REC_COMP_ERR:      SEC_REC_COMP_ERR,
    SEC_REC_NORM_DDAM_ERR: SEC_REC_COMP_DDAM_ERR,
    SEC_REC_COMP_DDAM_ERR: SEC_REC_COMP_DDAM_ERR}    


# Map type to equivalent type with compression off
SEC_REC_COMPRESS_OFF = {
    SEC_REC_NONE:          SEC_REC_NONE,
    SEC_REC_NORM:          SEC_REC_NORM,
    SEC_REC_COMP:          SEC_REC_NORM,
    SEC_REC_NORM_DDAM:     SEC_REC_NORM_DDAM,
    SEC_REC_COMP_DDAM:     SEC_REC_NORM_DDAM,
    SEC_REC_NORM_ERR:      SEC_REC_NORM_ERR,
    SEC_REC_COMP_ERR:      SEC_REC_NORM_ERR,
    SEC_REC_NORM_DDAM_ERR: SEC_REC_NORM_DDAM_ERR,
    SEC_REC_COMP_DDAM_ERR: SEC_REC_NORM_DDAM_ERR}    


class sector(bytearray):
    """Class representing one sector of an ImageDisk disk image."""
    
    sector_type = SEC_REC_NORM
    sec_num  = 0
    cyl_num  = 0
    head_num = 0
    
    def __init__(self,
                 orig       = None,
                 size       = 512,
                 fill       = 0xFF,
                 cyl_num    = 0,
                 head_num   = 0,
                 sec_num    = 0):

        """sector class initializer.

        If orig is not None, then initialize instance as a copy of the
        passed sector object and ignore all other arguments.

        cyl_num and head_num are the cylinder and head numbers to be
        stored in sector headers, which are usually but not
        always the actual physical cylinder and head numbers.

        sec_num is the sector number to be stored in the sector header.
        It is commonly different from the physical sector number due to
        interleaving, 1-based sector numbering, etc."""

        if orig is not None:
            # Copy constructor
            self.copy(orig)

        else:
            self.cyl_num  = cyl_num
            self.head_num = head_num
            self.sec_num  = sec_num

            self.extend([fill]*size)
    
    def copy(self, orig):
        """Replace current contents with a copy of another instance."""

        if len(self) > 0:
            del self[:]

        self.sector_type = orig.sector_type
        self.sec_num     = orig.sec_num
        self.cyl_num     = orig.cyl_num
        self.head_num    = orig.head_num
        
        self.extend(orig)


    def is_compressible(self):
        'Determine if sector data consists of only one repeated byte.'

        if len(self) < 2:
            return False
        for b in self[1:]:
            if b != self[0]:
                return False
        return True


    def is_ddam(self):
        'Return True if sector has deleted data address mark.'
        return (self.sector_type in SEC_REC_DDAM_TYPES)


    def is_error(self):
        'Return True if sector has known errors.'
        return (self.sector_type in SEC_REC_ERR_TYPES)


    def details(self):
        """Returns detailed summary of sector contents.

        Format is cylnum.headnum.secnum
        Flags may be appended:

        !: Error while reading sector
        *: Sector with DDAM (deleted data address mark)
        c: Sector is compressible (contains single repeated byte)
        """

        desc = '{:d}.{:d}.{:d}'.format(self.cyl_num,
                                       self.head_num,
                                       self.sec_num)

        if self.sector_type in SEC_REC_DDAM_TYPES:
            desc = desc + '*'

        if self.sector_type in SEC_REC_ERR_TYPES:
            desc = desc + '!'

        if self.is_compressible():
            desc = desc + 'c'

        return desc


    def hexdump(self):
        """Returns string containing hexadecimal + ASCII dump of
        sector contents."""

        dump = ''
        last = len(self) - 1
        for n, b in enumerate(self):
            if (n % 16) == 0:
                offset = '{:04X}: '.format(n)
                hexfld = ''
                ascfld = ''
            hexfld = hexfld + '{:02X}'.format(b)
            if (n % 4) == 3:
                hexfld = hexfld + '  '
            c = chr(b)
            if c in __printable__:
                ascfld = ascfld + c
            else:
                ascfld = ascfld + ' '
            #if (n % 4) == 3:
            #    ascfld = ascfld + '  '
            if ((n % 16) == 15) or (n == last):
                dump = dump + '{:5s} {:38s}|  {:s}\n'.format(offset,
                                                             hexfld,
                                                             ascfld)
        return dump
