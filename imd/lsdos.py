#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""This package defines TRS-80 LS-DOS filesystem objects."""

from . import disk
from . import track
from . import filesystem
import collections

# Named tuple of parameters defining variants of this filesystem:
#  cyl0mode:    Track mode for cylinder 0
#  mode:        Track mode for all other cylinders
#  heads:       Number of heads (1 or 2)
#  cylinders:   Number of cylinders (typically 35, 40, 77 or 80)
#  secspergran: Sectors per granule
#  granspercyl: Granules per cylinder
#  dirsecs:     Directory sectors
#  skip:        Interleave skip factor

lsdos_params = collections.namedtuple('lsdos_params',
    'cyl0mode mode heads cylinders secspergran granspercyl dirsecs skip')

# Filesystem dictionary defines all of the supported LSDOS filesystems
# that may be requested:
lsdos_fsdict = {
    # Note: LSDOS interleave skips are guessed. Track modes may also be
    # wrong. All need to be verified. Maybe add dual-density variants?
    'lsdos-3p5-dsdd-80':  filesystem.fsdesc('LS-DOS 3.5" DSDD 80 cylinder',  lsdos, lsdos_params(track.MODE_250K_MFM, track.MODE_250K_MFM, 2, 80, 6,  6, 32, 2)),
    'lsdos-5p25-sssd-35': filesystem.fsdesc('LS-DOS 5.25" SSSD 35 cylinder', lsdos, lsdos_params(track.MODE_250K_FM,  track.MODE_250K_FM,  1, 35, 5,  2, 8,  2)),
    'lsdos-5p25-dssd-35': filesystem.fsdesc('LS-DOS 5.25" DSSD 35 cylinder', lsdos, lsdos_params(track.MODE_250K_FM,  track.MODE_250K_FM,  2, 35, 5,  4, 18, 2)),
    'lsdos-5p25-ssdd-35': filesystem.fsdesc('LS-DOS 5.25" SSDD 35 cylinder', lsdos, lsdos_params(track.MODE_250K_MFM, track.MODE_250K_MFM, 1, 35, 6,  3, 16, 2)),
    'lsdos-5p25-dsdd-35': filesystem.fsdesc('LS-DOS 5.25" DSDD 35 cylinder', lsdos, lsdos_params(track.MODE_250K_MFM, track.MODE_250K_MFM, 2, 35, 6,  6, 32, 2)),
    'lsdos-5p25-sssd-40': filesystem.fsdesc('LS-DOS 5.25" SSSD 40 cylinder', lsdos, lsdos_params(track.MODE_250K_FM,  track.MODE_250K_FM,  1, 40, 5,  2, 8,  2)),
    'lsdos-5p25-dssd-40': filesystem.fsdesc('LS-DOS 5.25" DSSD 40 cylinder', lsdos, lsdos_params(track.MODE_250K_FM,  track.MODE_250K_FM,  2, 40, 5,  4, 18, 2)),
    'lsdos-5p25-ssdd-40': filesystem.fsdesc('LS-DOS 5.25" SSDD 40 cylinder', lsdos, lsdos_params(track.MODE_250K_MFM, track.MODE_250K_MFM, 1, 40, 6,  3, 16, 2)),
    'lsdos-5p25-dsdd-40': filesystem.fsdesc('LS-DOS 5.25" DSDD 40 cylinder', lsdos, lsdos_params(track.MODE_250K_MFM, track.MODE_250K_MFM, 2, 40, 6,  6, 32, 2)),
    'lsdos-5p25-sssd-80': filesystem.fsdesc('LS-DOS 5.25" SSSD 80 cylinder', lsdos, lsdos_params(track.MODE_250K_FM,  track.MODE_250K_FM,  1, 80, 5,  2, 8,  2)),
    'lsdos-5p25-dssd-80': filesystem.fsdesc('LS-DOS 5.25" DSSD 80 cylinder', lsdos, lsdos_params(track.MODE_250K_FM,  track.MODE_250K_FM,  2, 80, 5,  4, 18, 2)),
    'lsdos-5p25-ssdd-80': filesystem.fsdesc('LS-DOS 5.25" SSDD 80 cylinder', lsdos, lsdos_params(track.MODE_250K_MFM, track.MODE_250K_MFM, 1, 80, 6,  3, 16, 2)),
    'lsdos-5p25-dsdd-80': filesystem.fsdesc('LS-DOS 5.25" DSDD 80 cylinder', lsdos, lsdos_params(track.MODE_250K_MFM, track.MODE_250K_MFM, 2, 80, 6,  6, 32, 2)),
    'lsdos-8-sssd-77':    filesystem.fsdesc('LS-DOS 8" SSSD 77 cylinder',    lsdos, lsdos_params(track.MODE_500K_FM,  track.MODE_500K_MFM, 1, 77, 8,  2, 14, 4)),
    'lsdos-8-dssd-77':    filesystem.fsdesc('LS-DOS 8" DSSD 77 cylinder',    lsdos, lsdos_params(track.MODE_500K_FM,  track.MODE_500K_MFM, 2, 77, 8,  4, 30, 4)),
    'lsdos-8-ssdd-77':    filesystem.fsdesc('LS-DOS 8" SSDD 77 cylinder',    lsdos, lsdos_params(track.MODE_500K_FM,  track.MODE_500K_MFM, 1, 77, 10, 3, 28, 4)),
    'lsdos-8-dsdd-77':    filesystem.fsdesc('LS-DOS 8" DSDD 77 cylinder',    lsdos, lsdos_params(track.MODE_500K_FM,  track.MODE_500K_MFM, 2, 77, 10, 6, 32, 4))}


def lsdos(filesystem):
    """Class representing a TRS-80 LS-DOS filesystem.

    This is a template for function classes which will implement actual
    filesystems.
    """

    params = None

    def __init__(self,
                 orig     = None,
                 filename = None,
                 params   = lsdos_fsdict['lsdos-5p25-ssdd-40']):
        """Creates an LS-DOS filesystem.

        If orig is another instance of this class, then create a copy of
        it. If orig is a disk class, copy sector data and try to infer
        parameters. Otherwise, create an empty, formatted filesystem.

        The params argument must be an lsdosparams named tuple defining
        the filesystem parameters. If not provided, the filesystem
        defaults to LS-DOS 5.25 inch SSDD 40 cylinders, as appropriate
        for a typical TRS-80 Model III or 4.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def examine(self):
        """Examine sector contents to infer parameters.
        Set parameters if valid, or raise exception if not valid.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def init(self,
             params = defparams):
        """Replace contents with image of blank formatted disk.

        Pass a class-specific tuple of parameters with the params
        argument to select a specific version of a filesystem. Argument
        is ignored if filesystem does not offer any variations. Each
        class should set a reasonable default value for this argument.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def files(self):
        """Return list of names of files on the disk.

        If filesystem supports subdirectories, then listing will be
        recursive, with paths relative to root of filesystem. Empty
        directories are not included.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def dirs(self):
        """Return list of names of directories on the disk (if applicable).

        List includes all directories, with paths relative to root of
        filesystem. If filesystem does not support subdirectories, then
        will return an empty list.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def listing(self):
        """Return a printable listing of the disk contents.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def addfile(self, filename, data):
        """Add a new file to the filesystem.

        If filesystem supports subdirectories, directories in filesystem
        path will be created if they do not already exist.

        data argument should be a bytearray containing desired file
        contents. If filesystem requires fixed block lengths, then data
        may be padded as needed.

        Filesystem-specific classes may include additional arguments for
        permissions, timestamps, etc. as appropriate, with reasonable
        default values.
        
        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def delfile(self, filename):
        """Delete the specified file from the filesystem.
        
        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def readfile(self, filename):
        """Return contents of specified file on filesystem.

        Returns a bytearray.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def getbootblocks(self):
        """Returns contents of filesystem boot blocks.

        Returns a bytearray if filesystem supports bootblocks, or None
        if not supported.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def setbootblocks(self, data):
        """Sets fiesystem bootblocks to provided data.

        Data should be a bytearray, and will be padded or truncated
        as needed.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def mkdir(self, dirname):
        """Create subdirectory with specified path.

        Raises exception if filesystem does not support subdirectories.

        Recursively creates entire path as necessary.

        Filesystem-specific classes may include additional arguments for
        permissions, timestamps, etc. as appropriate, with reasonable
        default values.
        
        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def deldir(self, dirname):
        """Deletes the specified directory from the filesystem.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')



