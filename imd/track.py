#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016-2019 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""This package defines track objects stored by the cylinder class."""

from . import sector
from . import util
import textwrap

# Track mode codes
MODE_500K_FM  = 0x00
MODE_300K_FM  = 0x01
MODE_250K_FM  = 0x02
MODE_500K_MFM = 0x03
MODE_300K_MFM = 0x04
MODE_250K_MFM = 0x05

MODE_NAMES = {
    MODE_500K_FM:  '500kbps FM',
    MODE_300K_FM:  '300kbps FM',
    MODE_250K_FM:  '250kbps FM',
    MODE_500K_MFM: '500kbps MFM',
    MODE_300K_MFM: '300kbps MFM',
    MODE_250K_MFM: '250kbps MFM'}
              

# Sector size codes
SEC_SIZE_128  = 0x00
SEC_SIZE_256  = 0x01
SEC_SIZE_512  = 0x02
SEC_SIZE_1024 = 0x03
SEC_SIZE_2048 = 0x04
SEC_SIZE_4096 = 0x05
SEC_SIZE_8192 = 0x06

SEC_SIZE_FROM_CODE = {SEC_SIZE_128:   128,
                      SEC_SIZE_256:   256,
                      SEC_SIZE_512:   512,
                      SEC_SIZE_1024: 1024,
                      SEC_SIZE_2048: 2048,
                      SEC_SIZE_4096: 4096,
                      SEC_SIZE_8192: 8192}

SEC_CODE_FROM_SIZE = {128:  SEC_SIZE_128,
                      256:  SEC_SIZE_256,
                      512:  SEC_SIZE_512,
                      1024: SEC_SIZE_1024,
                      2048: SEC_SIZE_2048,
                      4096: SEC_SIZE_4096,
                      8192: SEC_SIZE_8192}


# Flags in upper bits of head value
SEC_CYL_MAP_FLAG  = 0x80
SEC_HEAD_MAP_FLAG = 0x40
HEAD_MASK         = 0x3F



    
class track(list):
    """Class representing one track of an ImageDisk disk image.

    This class is an ordered list of sector class instances, with the
    indices corresponding to physical sector numbers, starting at zero
    for the sector immediately after the index pulse. Each element of
    the list shall be either a sector class instance.
    """


    mode         = MODE_250K_MFM
    sector_size  = 512
    cyl_num      = 0
    head_num     = 0


    def __init__(self,
                 orig       = None,
                 sectors    = 0,
                 mode       = MODE_250K_MFM,
                 start      = 0,
                 skip       = 0,
                 skew       = 0,
                 size       = 512,
                 fill       = 0xFF,
                 cyl_num    = 0,
                 head_num   = 0):

        """track class initializer.

        If orig is not None, then initialize instance as a copy of the
        passed track object and ignore all other arguments.

        cyl_num and head_num are the cylinder and head numbers to be
        stored in sector headers, which are usually but not
        always the actual physical cylinder and head numbers."""

        if orig is not None:
            self.copy(orig)

        else:
            self.mode        = mode
            self.sector_size = size
            self.cyl_num     = cyl_num
            self.head_num    = head_num

            for n in range(start, start + sectors):
                self.append(sector.sector(size     = size,
                                          fill     = fill,
                                          cyl_num  = cyl_num,
                                          head_num = head_num,
                                          sec_num  = n))

            self.interleave(skip)
            self.skew(skew)


    def copy(self, orig):
        """Replace current contents with a copy of another instance."""

        if len(self) > 0:
            del self[:]

        self.mode        = orig.mode
        self.sector_size = orig.sector_size
        self.cyl_num     = orig.cyl_num
        self.head_num    = orig.head_num

        for sec in orig:
            self.append(sector.sector(sec))




    def needs_cyl_map(self):
        'Return True if track requires a cylinder map.'

        for sec in self:
            if sec.cyl_num != self.cyl_num:
                return True
        return False
    

    def needs_head_map(self):
        'Return True if track requires a head map.'

        for sec in self:
            if sec.head_num != self.head_num:
                return True
        return False
    

    def details(self):
        """Returns detailed summary of track contents.

        * after cylinder.head.sector number indicates deleted data
          address mark (DDAM).

        ! after cylinder.head.sector number indicates error encountered
          when reading the sector.

        c after cylinder.head.sector number indicates compressible
          sector.
        """

        compressible_flag = False
        ddam_flag         = False
        error_flag        = False

        desc = '{:d} byte, {:d} sectors, {:s}'.format(
            self.sector_size, len(self), MODE_NAMES[self.mode])

        if self.needs_cyl_map():
            desc = desc + ', CYL_MAP'

        if self.needs_head_map():
            desc = desc + ', HEAD_MAP'

        desc = desc + '\n'

        seclist = []
        for sec in self:
            seclist.append(sec.details())
            if sec.is_compressible():
                compressible_flag = True
            if sec.is_ddam():
                ddam_flag = True
            if sec.is_error():
                error_flag = True
                
        desc = desc + textwrap.fill(str(seclist).replace("'", "") + '\n', 72)

        if compressible_flag:
            desc = desc + '\nHas compressible sectors.'

        if ddam_flag:
            desc = desc + '\nHas DDAM sectors.'

        if error_flag:
            desc = desc + '\nHAS SECTORS WITH ERRORS!'

        return desc


    def interleave(self, skip=0):
        """Interleave sectors by specified skip factor.

        Set skip factor to 0 to remove interleave."""

        assert(skip >= 0)

        # Start by removing interleave. If skip == 0 then we're done.
        # Otherwise, doing this ensures that new interleaved order
        # is consistent.
        self.sort(key=lambda x: x.sec_num)

        if skip > 0:
            # Precompute length of list, minimum sector number,
            # and skip factor.
            l = len(self)
            m = min([x.sec_num for x in self])
            s = skip + 1

            # Sort the list with the new skip factor applied.
            self.sort(key=lambda x: ((x.sec_num-m)*s)%l)


    def skew(self, sec_num=0):
        """Rearrange the track such that the sector with the specified
        number occurs after the index pulse. Sector order is preserved.
        If sector with matching number is not found, track will be left
        unmodified. If more than one sector has the same sector number
        assigned, the first one found will be used."""

        for i,x in enumerate(self):
            if x.sec_num == sec_num:
                newlist = self[i:] + self[:i]
                del self[0:len(self)]
                self.extend(newlist)
                break
            
    def find_sector(self, sec):
        """Return sector with specified logical sector number.

        Returns None if sector cannot be found."""

        for s in self:
            if s.sec_num == sec:
                return s
        return None


