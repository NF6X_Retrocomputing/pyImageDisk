#!/usr/bin/env python
#
##########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Generate special disk images for testing pyImageDisk."""

##########################################################################
##########################################################################
# Make a carefully crafted image with:
# * All track modes
# * All sector types
# * Missing tracks
# * Various skews and interleaves
# * With and without cylinder maps and head maps
##########################################################################
##########################################################################

startpos = 0
endpos   = 0
print('File start')

imgfile = open('test_valid.imd', 'wb')
##########################################################################
# ASCII header

imgfile.write(b'pyImageDisk Test Image\x0D\x0A\x1A')
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('comment', startpos, endpos))
startpos = endpos + 1

##########################################################################
# cyl 0 head 0: MODE_250K_MFM SEC_SIZE_128 no cyl map no head map 9 sectors
# 0 interleave 0 skew

print('0.0')

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x05, 0x00, 0x00, 0x09, 0x00]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('track header', startpos, endpos))
startpos = endpos + 1

# sector map
imgfile.write(bytearray([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector map', startpos, endpos))
startpos = endpos + 1

# 0: sector data unavailable
imgfile.write(bytearray([0x00]))

# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x80))))

# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0x55]))

# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0x7F, -1, -1))))

# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0xAA]))

# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, 0x7F, -1))))

# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0x42]))

# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x80, 0x100, 1))))

# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x69]))

endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector data', startpos, endpos))
startpos = endpos + 1


##########################################################################
# cyl 0 head 1: MODE_300K_MFM SEC_SIZE_256 with cyl map no head map 9 sectors
# 0 interleave 3 skew

print('0.1')

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x04, 0x00, 0x81, 0x09, 0x01]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('track header', startpos, endpos))
startpos = endpos + 1

# sector map
imgfile.write(bytearray([0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x00, 0x01, 0x02]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector map', startpos, endpos))
startpos = endpos + 1

# cylinder map
imgfile.write(bytearray([0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('cylinder map', startpos, endpos))
startpos = endpos + 1

# 0: sector data unavailable
imgfile.write(bytearray([0x00]))

# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x100))))

# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0xAA]))

# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))

# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0x55]))

# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))

# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0x69]))

# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x00, 0x100, 1))))

# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x69]))

endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector data', startpos, endpos))
startpos = endpos + 1


##########################################################################
# cyl 1 head 0: MODE_500K_MFM SEC_SIZE_512 no cyl map with head map 9 sectors
# 2 interleave 0 skew

print('1.0')

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x03, 0x01, 0x40, 0x09, 0x02]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('track header', startpos, endpos))
startpos = endpos + 1

# sector map
imgfile.write(bytearray([0x00, 0x03, 0x06, 0x01, 0x04, 0x07, 0x02, 0x05, 0x08]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector map', startpos, endpos))
startpos = endpos + 1

# head map
imgfile.write(bytearray([0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('head map', startpos, endpos))
startpos = endpos + 1

# 0: sector data unavailable
imgfile.write(bytearray([0x00]))

# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))

# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0xDE]))

# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))

# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0xFC]))

# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))

# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0xDE]))

# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))

# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x69]))

endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector data', startpos, endpos))
startpos = endpos + 1

##########################################################################
# cyl 2 head 0: MODE_250K_FM SEC_SIZE_1024 with cyl map with head map 9 sectors
# 1 interleave 3 skew start at 2

print('2.0')

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x02, 0x02, 0xC0, 0x09, 0x03]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('track header', startpos, endpos))
startpos = endpos + 1

# sector map
imgfile.write(bytearray([0x03, 0x05, 0x07, 0x09, 0x02, 0x04, 0x06, 0x08, 0x0A]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector map', startpos, endpos))
startpos = endpos + 1

# cylinder map
imgfile.write(bytearray([0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('cylinder map', startpos, endpos))
startpos = endpos + 1

# head map
imgfile.write(bytearray([0x00, 0x01, 0x01, 0x01, 0x00, 0x00, 0x01, 0x01, 0x01]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('head map', startpos, endpos))
startpos = endpos + 1

# 0: sector data unavailable
imgfile.write(bytearray([0x00]))

# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))

# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0x12]))

# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))

# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0x37]))

# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))

# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0x7F]))

# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))

# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x22]))

endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector data', startpos, endpos))
startpos = endpos + 1


##########################################################################
# cyl 2 head 1: MODE_300K_FM SEC_SIZE_2048 no cyl map no head map 9 sectors
# 0 interleave 1 skew start 1

print('2.1')

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x01, 0x02, 0x01, 0x09, 0x04]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('track header', startpos, endpos))
startpos = endpos + 1

# sector map
imgfile.write(bytearray([0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector map', startpos, endpos))
startpos = endpos + 1

# 0: sector data unavailable
imgfile.write(bytearray([0x00]))

# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))

# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0x34]))

# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))

# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0x37]))

# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))

# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0xF7]))

# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))

# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x33]))

endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector data', startpos, endpos))
startpos = endpos + 1


##########################################################################
# cyl 4 head 0: MODE_500K_FM SEC_SIZE_4096 no cyl map no head map 9 sectors
# 0 interleave 0 skew

print('4.0')

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x00, 0x04, 0x00, 0x09, 0x05]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('track header', startpos, endpos))
startpos = endpos + 1

# sector map
imgfile.write(bytearray([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector map', startpos, endpos))
startpos = endpos + 1

# 0: sector data unavailable
imgfile.write(bytearray([0x00]))

# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))

# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0x34]))

# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))

# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0x10]))

# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))

# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0x01]))

# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))

# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x77]))

endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector data', startpos, endpos))
startpos = endpos + 1


##########################################################################
# cyl 5 head 1: MODE_250K_MFM SEC_SIZE_8192 no cyl map no head map 9 sectors
# 0 interleave 0 skew

print('5.1')

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x05, 0x05, 0x01, 0x09, 0x06]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('track header', startpos, endpos))
startpos = endpos + 1

# sector map
imgfile.write(bytearray([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08]))
endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector map', startpos, endpos))
startpos = endpos + 1

# 0: sector data unavailable
imgfile.write(bytearray([0x00]))

# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))

# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0x34]))

# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))

# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0x10]))

# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))
imgfile.write(bytearray(list(range(0xFF, -1, -1))))

# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0x01]))

# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))
imgfile.write(bytearray(list(range(0x100))))

# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x77]))

endpos = imgfile.tell() - 1
print('  {:16s} at offsets {:d}...{:d}'.format('sector data', startpos, endpos))
startpos = endpos + 1

print('End of file')

imgfile.close()


##########################################################################
##########################################################################
# Make image with invalid track mode.
##########################################################################
##########################################################################

imgfile = open('test_invalid_track_mode.imd', 'wb')
imgfile.write(b'pyImageDisk Test Image\x0D\x0A\x1A')
# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x42, 0x00, 0x00, 0x09, 0x00]))
imgfile.close


##########################################################################
##########################################################################
# Make image with invalid sector_type.
##########################################################################
##########################################################################

imgfile = open('test_invalid_sector_type.imd', 'wb')
imgfile.write(b'pyImageDisk Test Image\x0D\x0A\x1A')
# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x05, 0x00, 0x00, 0x09, 0x00]))
# sector map
imgfile.write(bytearray([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08]))
# 0: invalid sector type
imgfile.write(bytearray([0x42]))
imgfile.close



##########################################################################
##########################################################################
# Make image with duplicate track.
##########################################################################
##########################################################################

imgfile = open('test_duptrack.imd', 'wb')

##########################################################################
# ASCII header

imgfile.write(b'pyImageDisk Test Image\x0D\x0A\x1A')

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x05, 0x00, 0x00, 0x09, 0x00]))
# sector map
imgfile.write(bytearray([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08]))
# 0: sector data unavailable
imgfile.write(bytearray([0x00]))
# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x80))))
# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0x55]))
# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0x7F, -1, -1))))
# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0xAA]))
# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, 0x7F, -1))))
# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0x42]))
# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x80, 0x100, 1))))
# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x69]))

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x05, 0x00, 0x00, 0x09, 0x00]))
# sector map
imgfile.write(bytearray([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08]))
# 0: sector data unavailable
imgfile.write(bytearray([0x00]))
# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x80))))
# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0x55]))
# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0x7F, -1, -1))))
# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0xAA]))
# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, 0x7F, -1))))
# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0x42]))
# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x80, 0x100, 1))))
# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x69]))

imgfile.close()

##########################################################################
##########################################################################
# Make image with:
# * Multiple track lengths
# * Single sector size
# * Single track mode
##########################################################################
##########################################################################

imgfile = open('test_valid2.imd', 'wb')

##########################################################################
# ASCII header

imgfile.write(b'pyImageDisk Test Image\x0D\x0A\x1A')

##########################################################################
# cyl 0 head 0: MODE_250K_MFM SEC_SIZE_128 no cyl map no head map 9 sectors
# 0 interleave 0 skew

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x05, 0x00, 0x00, 0x09, 0x00]))

# sector map
imgfile.write(bytearray([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08]))

# 0: sector data unavailable
imgfile.write(bytearray([0x00]))

# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x80))))

# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0x55]))

# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0x7F, -1, -1))))

# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0xAA]))

# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, 0x7F, -1))))

# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0x42]))

# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x80, 0x100, 1))))

# 8: compressed data with error and DDAM
imgfile.write(bytearray([0x08]))
imgfile.write(bytearray([0x69]))


##########################################################################
# cyl 0 head 1: MODE_250K_MFM SEC_SIZE_128 no cyl map no head map 9 sectors
# 0 interleave 0 skew

# mode, cyl, head, num_secs, sec_size
imgfile.write(bytearray([0x05, 0x01, 0x00, 0x08, 0x00]))

# sector map
imgfile.write(bytearray([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]))

# 0: sector data unavailable
imgfile.write(bytearray([0x00]))

# 1: normal data
imgfile.write(bytearray([0x01]))
imgfile.write(bytearray(list(range(0x80))))

# 2: compressed
imgfile.write(bytearray([0x02]))
imgfile.write(bytearray([0x55]))

# 3: normal data with DDAM
imgfile.write(bytearray([0x03]))
imgfile.write(bytearray(list(range(0x7F, -1, -1))))

# 4: compressed with DDAM
imgfile.write(bytearray([0x04]))
imgfile.write(bytearray([0xAA]))

# 5: normal data with error
imgfile.write(bytearray([0x05]))
imgfile.write(bytearray(list(range(0xFF, 0x7F, -1))))

# 6: compressed data with error
imgfile.write(bytearray([0x06]))
imgfile.write(bytearray([0x42]))

# 7: normal data with error and DDAM
imgfile.write(bytearray([0x07]))
imgfile.write(bytearray(list(range(0x80, 0x100, 1))))

imgfile.close()
