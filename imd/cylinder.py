#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""This package defines cylinder objects stored by the disk class."""

from . import track
from . import util


class cylinder(list):
    """Class representing one cylinder of an ImageDisk disk image.

    This class is an ordered list of track class instances, with the
    indices corresponding to physical head numbers, starting at zero.
    Each element of the list shall be either a track class instance or
    None."""

    def __init__(self,
                 orig       = None,
                 heads      = 0,
                 sectors    = 0,
                 mode       = track.MODE_250K_MFM,
                 start      = 0,
                 skip       = 0,
                 skew       = 0,
                 size       = 512,
                 fill       = 0xFF,
                 cyl_num    = 0):

        """cylinder class initializer.
                 
        If orig is not None, then initialize instance as a copy of the
        passed cylinder object and ignore all other arguments.

        cyl_num is the cylinder number to be stored in sector headers,
        which is usually but not always the actual physical cylinder
        number."""
        
        if orig is not None:
            # Copy constructor
            self.copy(orig)
        
        else:
            for n in range(heads):
                self.append(track.track(sectors  = sectors,
                                        mode     = mode,
                                        start    = start,
                                        skip     = skip,
                                        skew     = skew,
                                        size     = size,
                                        fill     = fill,
                                        cyl_num  = cyl_num,
                                        head_num = n))

    def copy(self, orig):
        """Replace current contents with a copy of another instance."""

        if len(self) > 0:
            del self[:]

        for trk in orig:
            if trk is not None:
                self.append(track.track(trk))
            else:
                self.append(None)




