#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Unit tests for imd.disk class and below."""

import unittest
import os
import pkg_resources
import tempfile
import filecmp

from .disk     import *
from .cylinder import *
from .track    import *
from .sector   import *

class test_imd_valid(unittest.TestCase):

    """Tests using hand-crafted valid image with most feature combinations."""

    imdfile    = None
    image      = None
    tmpfhandle = None
    tmpfname   = None
    

    def setUp(self):
        """Set up for each test."""
        self.imdfile = pkg_resources.resource_filename(__name__,
            'test_data/test_valid.imd')
        self.image   = disk(filename=self.imdfile)
        (self.tmpfhandle, self.tmpfname) \
            = tempfile.mkstemp(suffix='.imd', prefix='temp_image')


    def tearDown(self):
        """Clean up after each test."""
        pkg_resources.cleanup_resources()
        if self.tmpfhandle is not None:
            os.close(self.tmpfhandle)
            self.tmpfhandle = None
        if self.tmpfname is not None:
            os.unlink(self.tmpfname)
            self.tmpfname = None


    def test_summary(self):
        """Check for correct summary string."""
        expected_summary = pkg_resources.resource_string(__name__,
            'test_data/test_valid.imd.summary').decode()
        self.assertEqual(self.image.summary(), expected_summary)


    def test_details(self):
        """Check for correct details string."""
        expected_details = pkg_resources.resource_string(__name__,
            'test_data/test_valid.imd.details').decode()
        self.assertEqual(self.image.details(), expected_details)


    def test_disk_deskew(self):
        """Test removal of disk skew."""
        self.image.skew(0)
        expected_details = pkg_resources.resource_string(__name__,
            'test_data/test_valid_disk_deskew.imd.details').decode()
        self.assertEqual(self.image.details(), expected_details)


    def test_track_reskew(self):
        """Test change of single track skew."""
        self.image[0][1].skew(1)
        expected_details =  pkg_resources.resource_string(__name__,
            'test_data/test_valid_track_reskew.imd.details').decode()
        self.assertEqual(self.image.details(), expected_details)


    def test_disk_deinterleave(self):
        """Test removal of disk interleave."""
        self.image.interleave(0)
        expected_details = pkg_resources.resource_string(__name__,
            'test_data/test_valid_disk_deinterleave.imd.details').decode()
        self.assertEqual(self.image.details(), expected_details)


    def test_track_reinterleave(self):
        """Test change of single track interleave."""
        self.image[1][0].interleave(1)
        expected_details = pkg_resources.resource_string(__name__,
            'test_data/test_valid_track_reinterleave.imd.details').decode()
        self.assertEqual(self.image.details(), expected_details)


    def test_sector_dump_all(self):
        """Test full hexadecimal dump against hand-verified dump file."""
        expected_dump = pkg_resources.resource_string(__name__,
            'test_data/test_valid.imd.hexdump').decode()
        self.assertMultiLineEqual(self.image.dump_all(), expected_dump)


    def test_image_save(self):
        """Test saving disk image to file."""
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.image.save(self.tmpfname)
        self.assertTrue(filecmp.cmp(self.imdfile, self.tmpfname, False),
                       'Save file "{:s}" does not match load file "{:s}".'
                       .format(self.tmpfname, self.imdfile))


    def test_image_copy_save(self):
        """Test saving disk image to file, using copy constructor."""
        imagecopy = disk(self.image)
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        imagecopy.save(self.tmpfname)
        self.assertTrue(filecmp.cmp(self.imdfile, self.tmpfname, False),
                       'Save file "{:s}" does not match load file "{:s}".'
                       .format(self.tmpfname, self.imdfile))


    def test_image_copyover_save(self):
        """Test saving disk image to file, using copy over existing image."""

        imagecopy = disk(comment='Test', cylinders=3, heads=2, sectors=8,
                         mode=MODE_300K_MFM, start=5, skip=1, skew=7,
                         size=128, fill=0x42)
        imagecopy.copy(self.image)
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        imagecopy.save(self.tmpfname)
        self.assertTrue(filecmp.cmp(self.imdfile, self.tmpfname, False),
                       'Save file "{:s}" does not match load file "{:s}".'
                       .format(self.tmpfname, self.imdfile))


    def test_sector_copy(self):
        """Test sector.copy()."""
        srcsec  = self.image.find_sector(2, 0, 9)
        destsec = sector(size=256, fill=0x69, cyl_num=7, head_num=1, sec_num=3)
        self.assertEqual('7.1.3c', destsec.details())
        destsec.copy(srcsec)
        self.assertEqual(srcsec.details(), destsec.details())
        self.assertEqual(srcsec.hexdump(), destsec.hexdump())


    def test_track_copy(self):
        """Test track.copy()."""
        srctrack  = self.image[0][0]
        desttrack = track(sectors=3, mode=MODE_500K_MFM, start=2, skip=3,
                          skew=4, size=1024, fill=0x12, cyl_num=5, head_num=1)
        desttrack.copy(srctrack)
        self.assertEqual(srctrack.details(), desttrack.details())
        self.assertEqual(srctrack[1].hexdump(), desttrack[1].hexdump())


    def test_cylinder_copy(self):
        """Test cylinder.copy()."""
        srccyl  = self.image[1]
        destcyl = cylinder(heads=2, sectors=2, mode=MODE_300K_FM, start=1,
                           skip=2, skew=3, size=512, fill=0x34, cyl_num=9)
        destcyl.copy(srccyl)
        self.assertEqual(srccyl[0].details(), destcyl[0].details())
        self.assertEqual(srccyl[0][1].hexdump(), destcyl[0][1].hexdump())


    def test_find_sector(self):
        """Test disk.find_sector()."""
        # Find a sector
        self.assertEqual(self.image.find_sector(2, 1, 4).details(), '2.1.4*')
        # Missing sector on valid track
        self.assertIsNone(self.image.find_sector(2, 0, 1))
        # Missing track
        self.assertIsNone(self.image.find_sector(1, 1, 0))
        # Missing cylinder
        self.assertIsNone(self.image.find_sector(3, 0, 0))


    def test_reload(self):
        """Test disk.load() with pre-existing image contents."""
        self.image = disk(comment='Test', cylinders=3, heads=2, sectors=8,
                          mode=MODE_300K_MFM, start=5, skip=1, skew=7,
                          size=128, fill=0x42)
        self.image.load(self.imdfile)
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.image.save(self.tmpfname)
        self.assertTrue(filecmp.cmp(self.imdfile, self.tmpfname, False),
                       'Save file "{:s}" does not match load file "{:s}".'
                       .format(self.tmpfname, self.imdfile))

        

class test_imd_eof(unittest.TestCase):

    """Test handling of unexpected EOFs in image file.

    Each test truncates test_data/test_valid.imd at a different location
    to test handling of unexpected EOFs in different parts of the file.
    """

    imddata    = None
    tmpfhandle = None
    tmpfname   = None

    def setUp(self):
        """Set up for each test."""
        self.imddata = pkg_resources.resource_string(__name__,
            'test_data/test_valid.imd')
        (self.tmpfhandle, self.tmpfname) \
            = tempfile.mkstemp(suffix='.imd', prefix='temp_image')


    def tearDown(self):
        """Clean up after each test."""
        pkg_resources.cleanup_resources()
        assert(self.tmpfhandle is None)
        if self.tmpfname is not None:
            os.unlink(self.tmpfname)
            self.tmpfname = None


    def test_eof_comment(self):
        """Test handling of unexpected EOF in comment."""
        os.write(self.tmpfhandle, self.imddata[:10])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_cylnum(self):
        """Test handling of unexpected EOF in cylinder number."""
        os.write(self.tmpfhandle, self.imddata[:26])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_headnum(self):
        """Test handling of unexpected EOF in head number."""
        os.write(self.tmpfhandle, self.imddata[:27])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_secs(self):
        """Test handling of unexpected EOF in sector count."""
        os.write(self.tmpfhandle, self.imddata[:28])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_size(self):
        """Test handling of unexpected EOF in sector size."""
        os.write(self.tmpfhandle, self.imddata[:29])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_secmap(self):
        """Test handling of unexpected EOF in sector number map."""
        os.write(self.tmpfhandle, self.imddata[:32])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_cylmap(self):
        """Test handling of unexpected EOF in cylinder map."""
        os.write(self.tmpfhandle, self.imddata[:580])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_headmap(self):
        """Test handling of unexpected EOF in head map."""
        os.write(self.tmpfhandle, self.imddata[:1640])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_sectype(self):
        """Test handling of unexpected EOF in sector type."""
        os.write(self.tmpfhandle, self.imddata[:39])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_secdata(self):
        """Test handling of unexpected EOF in sector data."""
        os.write(self.tmpfhandle, self.imddata[:41])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)


    def test_eof_seccompdata(self):
        """Test handling of unexpected EOF in compressed sector data."""
        os.write(self.tmpfhandle, self.imddata[:170])
        os.close(self.tmpfhandle)
        self.tmpfhandle = None
        self.assertRaises(EOFError, disk, filename=self.tmpfname)



class test_imd_misc(unittest.TestCase):

    """Misc. tests, not using a common test image file."""

    tmpfname = None

    def setUp(self):
        """Set up for each test."""
        pass

    def tearDown(self):
        """Clean up after each test."""
        if self.tmpfname is not None:
            os.unlink(self.tmpfname)
            self.tmpfname = None
        pkg_resources.cleanup_resources()


    def test_duptrack(self):
        """Test handling of image with duplicate track."""
        imdfile = pkg_resources.resource_filename(__name__,
            'test_data/test_duptrack.imd')
        self.assertRaises(RuntimeError, disk, filename=imdfile)


    def test_summary2(self):
        """Check for correct summary string corner cases."""
        # Main test images misses coverage of:
        # * Multiple track lengths
        # * Single sector size
        # * Single track mode
        imdfile = pkg_resources.resource_filename(__name__,
            'test_data/test_valid2.imd')
        image = disk(filename=imdfile)
        expected_summary = pkg_resources.resource_string(__name__,
            'test_data/test_valid2.imd.summary').decode()
        self.assertEqual(image.summary(), expected_summary)


    def test_invalid_track_mode(self):
        """Test handling of invalid track mode."""
        imdfile = pkg_resources.resource_filename(__name__,
            'test_data/test_invalid_track_mode.imd')
        self.assertRaises(ValueError, disk, filename=imdfile)


    def test_invalid_sector_type(self):
        """Test handling of invalid sector type."""
        imdfile = pkg_resources.resource_filename(__name__,
            'test_data/test_invalid_sector_type.imd')
        self.assertRaises(ValueError, disk, filename=imdfile)


    def test_constructor(self):
        """Test constructor arguments."""
        image = disk(comment='Test', cylinders=3, heads=2, sectors=8,
                     mode=MODE_300K_MFM, start=5, skip=1, skew=7, size=128,
                     fill=0x42)
        expected_details = pkg_resources.resource_string(__name__,
            'test_data/test_constructor.imd.details').decode()
        self.assertEqual(image.details(), expected_details)
        expected_dump = pkg_resources.resource_string(__name__,
            'test_data/test_constructor.imd.hexdump').decode()
        self.assertMultiLineEqual(image.dump_all(), expected_dump)


    def test_magic(self):
        """Test magic number testing and fixing."""
        nomagic = pkg_resources.resource_filename(__name__,
            'test_data/test_nomagic.imd')
        addmagic = pkg_resources.resource_filename(__name__,
            'test_data/test_addmagic.imd')
        image = disk(filename=nomagic)
        (tmpfhandle, self.tmpfname) \
            = tempfile.mkstemp(suffix='.imd', prefix='temp_image')
        os.close(tmpfhandle)
        image.save(self.tmpfname)
        self.assertTrue(filecmp.cmp(addmagic, self.tmpfname, False),
                       'Save file "{:s}" does not match expected save file "{:s}".'
                       .format(self.tmpfname, addmagic))




def suite():
    """Return TestSuite containing all unit tests in this module."""
    s1 = unittest.TestLoader().loadTestsFromTestCase(test_imd_valid)
    s2 = unittest.TestLoader().loadTestsFromTestCase(test_imd_eof)
    s3 = unittest.TestLoader().loadTestsFromTestCase(test_imd_misc)
    return unittest.TestSuite([s1, s2, s3])

