#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Unit tests for imdutil script."""

import unittest
import sys
import os
import pkg_resources
import tempfile
import filecmp
import contextlib
import re

from .imdutil_main import *

try:
    # python 2
    from cStringIO import StringIO
except ImportError:
    # python 3
    from io import StringIO


# Inspired by:
# http://schinckel.net/2013/04/15/capture-and-test-sys.stdout-sys.stderr-in-unittest.testcase/
#
# But please do not bother asking me how it works. :)
@contextlib.contextmanager
def runmain(mainfunction, arglist):
    """Run mainfunction with arglist in sys.srgv, and capture stdout."""

    origargv, sys.argv   = sys.argv,   arglist
    origout,  sys.stdout = sys.stdout, StringIO()

    rtn = mainfunction()

    sys.stdout.seek(0)
    yield (rtn, sys.stdout.read())

    sys.stdout = origout
    sys.argv   = origargv


class test_imdutil_main(unittest.TestCase):

    """Test cases for imdutil script."""

    def setUp(self):
        """Set up for each test."""
        pass


    def tearDown(self):
        """Clean up after each test."""
        pkg_resources.cleanup_resources()


    def test_no_args(self):
        """Test invocation with no arguments."""

        with runmain(imdutil_main, ['imdutil.py']) as (rtn, capture):
            rtncode = rtn
            output  = capture

        # Nothing should have been printed on stdout.
        self.assertEqual(len(output), 0)

        # Check return code
        self.assertEqual(rtncode, 0)


    def test_help(self):
        """Test -h and --help options."""

        for flag in ['-h', '--help']:
            with runmain(imdutil_main, ['imdutil.py', flag]) as (rtn, capture):
                rtncode = rtn
                output  = capture

            # Look for some things that should show up in the help output.
            self.assertIsNotNone(re.search('Copyright',     output))
            self.assertIsNotNone(re.search('Example',       output))
            self.assertIsNotNone(re.search(imd.__version__, output))

            # Check return code
            self.assertEqual(rtncode, 0)


    def test_load_save(self):
        """Test -l, --load, -s, --save options."""

        imdfile = pkg_resources.resource_filename(__name__,
                                                  'test_data/test_valid.imd')
        (tmpfhandle, tmpfname) = tempfile.mkstemp(suffix='.imd',
                                                  prefix='temp_image')
        os.close(tmpfhandle)

        for (flag1, flag2) in [('-l', '-s'), ('--load', '--save')]:
            with runmain(imdutil_main,
                         ['imdutil.py', flag1, imdfile, flag2, tmpfname]) \
            as (rtn, capture):
                rtncode = rtn
                output  = capture

            # Nothing should have been printed on stdout.
            self.assertEqual(len(output), 0)

            # Check return code
            self.assertEqual(rtncode, 0)

            # Verify save file
            self.assertTrue(filecmp.cmp(imdfile, tmpfname, False))

            # Clean up temporary file
            os.unlink(tmpfname)


    def test_summary(self):
        """Test -S, --summary options."""

        imdfile = pkg_resources.resource_filename(__name__,
                                                  'test_data/test_valid.imd')
        expected_output = pkg_resources.resource_string(__name__,
            'test_data/test_valid.imd.summary').decode() + '\n'

        for flag in ['-S', '--summary']:
            with runmain(imdutil_main,
                         ['imdutil.py', '-l', imdfile, flag]) \
            as (rtn, capture):
                rtncode = rtn
                output  = capture

            # Check stdout
            self.assertEqual(output, expected_output)

            # Check return code
            self.assertEqual(rtncode, 0)


    def test_details(self):
        """Test -D, --details options."""

        imdfile = pkg_resources.resource_filename(__name__,
                                                  'test_data/test_valid.imd')
        expected_output = pkg_resources.resource_string(__name__,
            'test_data/test_valid.imd.details').decode() + '\n'

        for flag in ['-D', '--details']:
            with runmain(imdutil_main, ['imdutil.py', '-l', imdfile, flag]) \
            as (rtn, capture):
                rtncode = rtn
                output  = capture

            # Check stdout
            self.assertEqual(output, expected_output)

            # Check return code
            self.assertEqual(rtncode, 0)


    def test_dumpall(self):
        """Test -U, --dumpall options."""

        imdfile = pkg_resources.resource_filename(__name__,
                                                  'test_data/test_valid.imd')
        expected_output = pkg_resources.resource_string(__name__,
            'test_data/test_valid.imd.hexdump').decode() + '\n'

        for flag in ['-U', '--dumpall']:
            with runmain(imdutil_main, ['imdutil.py', '-l', imdfile, flag]) \
            as (rtn, capture):
                rtncode = rtn
                output  = capture

            # Check stdout
            self.assertEqual(output, expected_output)

            # Check return code
            self.assertEqual(rtncode, 0)


    def test_interleave(self):
        """Test -i, --interleave options."""

        imdfile = pkg_resources.resource_filename(__name__,
                                                  'test_data/test_valid.imd')
        expected_output = pkg_resources.resource_string(__name__,
            'test_data/test_valid_disk_deinterleave.imd.details').decode() + '\n'

        for flag in ['-i', '--interleave']:
            with runmain(imdutil_main,
                         ['imdutil.py', '-l', imdfile, flag, '0', '-D']) \
            as (rtn, capture):
                rtncode = rtn
                output  = capture

            # Check stdout
            self.assertEqual(output, expected_output)

            # Check return code
            self.assertEqual(rtncode, 0)


    def test_skew(self):
        """Test -k, --skew options."""

        imdfile = pkg_resources.resource_filename(__name__,
                                                  'test_data/test_valid.imd')
        expected_output = pkg_resources.resource_string(__name__,
            'test_data/test_valid_disk_deskew.imd.details').decode() + '\n'

        for flag in ['-k', '--skew']:
            with runmain(imdutil_main,
                         ['imdutil.py', '-l', imdfile, flag, '0', '-D']) \
            as (rtn, capture):
                rtncode = rtn
                output  = capture

            # Check stdout
            self.assertEqual(output, expected_output)

            # Check return code
            self.assertEqual(rtncode, 0)


    def test_dumpsector(self):
        """Test -u, --dumpsector options."""

        imdfile = pkg_resources.resource_filename(__name__,
                                                  'test_data/test_valid.imd')
        expected_output = pkg_resources.resource_string(__name__,
            'test_data/test_valid_0_1_4.imd.hexdump').decode() + '\n'

        for flag in ['-u', '--dumpsector']:
            with runmain(imdutil_main,
                         ['imdutil.py', '-l', imdfile, flag, '0.1.4']) \
            as (rtn, capture):
                rtncode = rtn
                output  = capture

            # Check stdout
            self.assertEqual(output, expected_output)

            # Check return code
            self.assertEqual(rtncode, 0)


    def test_comment(self):
        """Test -c, --comment options."""

        imdfile = pkg_resources.resource_filename(__name__,
                                                  'test_data/test_valid.imd')
        expected_output = pkg_resources.resource_string(__name__,
            'test_data/test_valid_comment.imd.summary').decode() + '\n'

        for flag in ['-c', '--comment']:
            with runmain(imdutil_main,
                         ['imdutil.py', '-l', imdfile, flag,
                          'comment\nchanged', '-S']) \
            as (rtn, capture):
                rtncode = rtn
                output  = capture

            # Check stdout
            self.assertEqual(output, expected_output)

            # Check return code
            self.assertEqual(rtncode, 0)


def suite():
    """Return TestSuite containing all unit tests in this module."""
    s1 = unittest.TestLoader().loadTestsFromTestCase(test_imdutil_main)
    return unittest.TestSuite([s1])

