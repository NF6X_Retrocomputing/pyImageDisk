#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Utility functions."""

import argparse

def indent(text, indent=4):
    """Indent lines of text by specified number of spaces."""

    return '\n'.join(' '*indent + line for line in text.splitlines()) + '\n'


def readord(filehandle, msg):
    """Read byte from filehandle and return ordinal value.

    Close filehandle and raise EOFError with message if
    end of file is encountered."""

    b = filehandle.read(1)
    if len(b) < 1:
        filehandle.close()
        raise EOFError(msg)
    else:
        return ord(b)


def readbytearray(filehandle, count, msg):
    """Read bytearray from filehandle.

    Close filehandle and raise EOFError with message if
    end of file is encountered."""

    ba = bytearray(filehandle.read(count))
    if len(ba) < count:
        filehandle.close()
        raise EOFError(msg)
    else:
        return ba


class gather_args(argparse.Action):
    """Accumulate arguments in order encountered."""

    def __call__(self, parser, namespace, values, option_string=None):
        if not 'arg_sequence' in namespace:
            setattr(namespace, 'arg_sequence', [])
        prev = namespace.arg_sequence
        prev.append((self.dest, values))
        setattr(namespace, 'arg_sequence', prev)


