#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2019 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""This package defines Tandy/TRS-80 Xenix filesystem objects."""

from . import disk
from . import track
from . import filesystem
import collections

# Named tuple of parameters defining variants of this filesystem:
#  cyl0mode:    Track mode for cylinder 0
#  mode:        Track mode for all other cylinders
#  heads:       Number of heads (1 or 2)
#  cylinders:   Number of cylinders (typically 77)
#  skip:        Interleave skip factor

xenix_params = collections.namedtuple('xenix_params',
                                      'cyl0mode mode heads cylinders skip')

# Filesystem dictionary defines all of the supported XENIX filesystems
# that may be requested:
xenix_fsdict = {
    # Note: XENIX interleave skips are guessed.
    'xenix-8-ss-77': filesystem.fsdesc(
        'LS-DOS 8" SS 77 cylinder', xenix, xenix_params(
            track.MODE_500K_FM,  track.MODE_500K_MFM, 1, 77, 4)),
    'xenix-8-ds-77': filesystem.fsdesc(
        'LS-DOS 8" DS 77 cylinder', xenix, xenix_params(
            track.MODE_500K_FM,  track.MODE_500K_MFM, 2, 77, 4))}


def xenix(filesystem):
    """Class representing a TandyTRS-80 Xenix filesystem."""

    params = None

    def __init__(self,
                 orig     = None,
                 filename = None,
                 params   = xenix_fsdict['xenix-8-ss-77']):
        """Create or copy a Xenix filesystem.

        If orig is another instance of this class, then create a copy of
        it. If orig is an instance of the disk class, copy sector data
        and try to infer parameters. Otherwise, create an empty,
        formatted filesystem.

        The params argument must be a xenixparams named tuple defining
        the filesystem parameters. If not provided, the filesystem
        defaults to a single-sided 8" Xenix filesystem. """

        if orig is not None:
            if type(orig) == type(self):
                # Copy another xenix filesystem
                raise exceptions.NotImplementedError('This is a stub for future development.')
            elif type(orig) == type(disk):
                # Copy an IMD image and infer parameters
                raise exceptions.NotImplementedError('This is a stub for future development.')
            else:
                # Inappropriate type
                raise exceptions.NotImplementedError('This is a stub for future development.')
        else if filename is not None:
            # Load image from .IMD file and infer parameters
            self.load(filename)
            self.examine()
        else:
            self.params = params
            raise exceptions.NotImplementedError('This is a stub for future development.')


    def examine(self):
        """Examine sector contents to infer parameters.
        Set parameters if valid, or raise exception if not valid.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def init(self,
             params = defparams):
        """Replace contents with image of blank formatted disk.

        Pass a class-specific tuple of parameters with the params
        argument to select a specific version of a filesystem. Argument
        is ignored if filesystem does not offer any variations. Each
        class should set a reasonable default value for this argument.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def files(self):
        """Return list of names of files on the disk.

        If filesystem supports subdirectories, then listing will be
        recursive, with paths relative to root of filesystem. Empty
        directories are not included.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def dirs(self):
        """Return list of names of directories on the disk (if applicable).

        List includes all directories, with paths relative to root of
        filesystem. If filesystem does not support subdirectories, then
        will return an empty list.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def listing(self):
        """Return a printable listing of the disk contents.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def addfile(self, filename, data):
        """Add a new file to the filesystem.

        If filesystem supports subdirectories, directories in filesystem
        path will be created if they do not already exist.

        data argument should be a bytearray containing desired file
        contents. If filesystem requires fixed block lengths, then data
        may be padded as needed.

        Filesystem-specific classes may include additional arguments for
        permissions, timestamps, etc. as appropriate, with reasonable
        default values.
        
        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def delfile(self, filename):
        """Delete the specified file from the filesystem.
        
        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def readfile(self, filename):
        """Return contents of specified file on filesystem.

        Returns a bytearray.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def getbootblocks(self):
        """Returns contents of filesystem boot blocks.

        Returns a bytearray if filesystem supports bootblocks, or None
        if not supported.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def setbootblocks(self, data):
        """Sets fiesystem bootblocks to provided data.

        Data should be a bytearray, and will be padded or truncated
        as needed.

        This virtual function must be defined in filesystem-specific
        classes."""
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def mkdir(self, dirname):
        """Create subdirectory with specified path.

        Raises exception if filesystem does not support subdirectories.

        Recursively creates entire path as necessary.

        Filesystem-specific classes may include additional arguments for
        permissions, timestamps, etc. as appropriate, with reasonable
        default values.
        
        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def deldir(self, dirname):
        """Deletes the specified directory from the filesystem.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')

