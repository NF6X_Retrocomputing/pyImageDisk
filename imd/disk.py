#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016-2019 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Support for creating and manipulating ImageDisk (.IMD) files."""

import warnings
from . import cylinder
from . import track
from . import sector
from . import util

_magic = 'IMD 1.18 file created by pyImageDisk'

class disk(list):
    """In-memory representation of a disk image to be loaded/saved in
    ImageDisk (.imd) format.

    This class is an ordered list of cylinder class instances, with
    some additional metadata such as the .imd file comment. List
    indices correspond to physical cylinder numbers, starting at zero.
    List members shall be either cylinder class instances, or None if
    the associated cylinder is not defined.
    """

    comment = ''

    def __init__(self,
                 orig       = None,
                 filename   = None,
                 comment    = _magic,
                 cylinders  = 0,
                 heads      = 0,
                 sectors    = 0,
                 mode       = track.MODE_250K_MFM,
                 start      = 0,
                 skip       = 0,
                 skew       = 0,
                 size       = 512,
                 fill       = 0xFF):

        """disk class initializer.

        If orig (e.g., supplied with a single positional argument) is
        not None, then initialize this class instance as a copy of orig.
        Other arguments are ignored.

        If orig=None and filename is set to a string containing the path
        to an ImageDisk (.imd) file, then initialize the class instance
        with the contents of that file and ignore other arguments.

        Otherwise, initialize the class instance as described by the
        other arguments.

        With no arguments, class instance will be empty.
        """

        if orig is not None:
            # Copy constructor
            self.copy(orig)

        elif filename is not None:
            # Load from file
            self.load(filename)

        else:
            # Initialize based on arguments, defaulting to empty image.
            self.comment = comment

            for n in range(cylinders):
                self.append(cylinder.cylinder(heads      = heads,
                                              sectors    = sectors,
                                              mode       = mode,
                                              start      = start,
                                              skip       = skip,
                                              skew       = skew,
                                              size       = size,
                                              fill       = fill,
                                              cyl_num    = n))


    def copy(self, orig):
        """Replace current contents with a copy of another instance."""

        if len(self) > 0:
            del self[:]

        self.comment = orig.comment

        for cyl in orig:
            if cyl is not None:
                self.append(cylinder.cylinder(cyl))
            else:
                self.append(None)


    def load(self, filename):
        """Read disk image from IMD file, replacing existing contents."""

        if len(self) > 0:
            del self[0:len(self)]

        fh = open(filename, 'rb')

        # Read comment, which is any text up to a ^Z.
        # Comment is optional, but ^Z is mandatory.
        # Raise warning if IMD magic number is not found, but continue
        # trying to load.
        # Remainder of file will be binary.
        self.comment = ''
        c=b''
        b=fh.read(1)
        while b != b'\x1A':
            if b == b'':
                fh.close()
                raise EOFError('Unexpected EOF while reading IMD comment.')
            c = c + b
            b=fh.read(1)
        self.comment = c.decode()

        # Change CR-LF to canonical newline
        self.comment = self.comment.replace('\x0D\x0A', '\n')
        # Remove trailing newline
        self.comment = self.comment.rstrip('\n')

        # Check for IMD magic number
        if (len(self.comment) < 3) or (self.comment[0:3] != 'IMD'):
            warnings.warn('IMD magic number not found.', Warning)

        # Read the tracks; first byte is track mode, or EOF.
        while True:
            trackstart = fh.tell() # Note position of track start
            b = fh.read(1)
            if len(b) != 1:
                # EOF encountered at a track boundary = normal end of file
                break
            
            trk = track.track()
            trk.mode = ord(b)
            if trk.mode not in track.MODE_NAMES:
                fh.close()
                raise ValueError('Invalid track mode 0x{:02x} at file offset'
                                 ' {:d}.'.format(trk.mode, trackstart))

            # Read cylinder, head, sector count, sector size
            cyl       = util.readord(fh,
                                     'Unexpected EOF while reading cylinder'
                                     ' number for track starting at offset'
                                     ' {:d}.'.format(trackstart))
            head      = util.readord(fh, 'Unexpected EOF while reading head'
                                     ' number for track starting at offset'
                                     ' {:d}.'.format(trackstart))
            secs      = util.readord(fh, 'Unexpected EOF while reading sector'
                                     ' count for track starting at offset'
                                     ' {:d}.'.format(trackstart))
            size_code = util.readord(fh, 'Unexpected EOF while reading sector'
                                     ' size for track starting at offset'
                                     ' {:d}.'.format(trackstart))

            trk.cyl_num     = cyl
            trk.head_num    = head & track.HEAD_MASK
            trk.sector_size = track.SEC_SIZE_FROM_CODE[size_code]

            # Read sector number map
            sec_map = util.readbytearray(fh, secs,
                                         'Unexpected EOF while reading sector'
                                         ' number map for track starting at'
                                         ' offset {:d}.'.format(trackstart))
            
            # Read optional cylinder map
            if head & track.SEC_CYL_MAP_FLAG:
                cyl_map = util.readbytearray(fh, secs,
                                            'Unexpected EOF while reading'
                                            ' cylinder map for track starting'
                                            ' at offset'
                                            ' {:d}.'.format(trackstart))
            else:
                cyl_map = [cyl]*secs
            
            # Read optional head map
            if head & track.SEC_HEAD_MAP_FLAG:
                head     = head & track.HEAD_MASK
                head_map = util.readbytearray(fh, secs,
                                              'Unexpected EOF while reading'
                                              ' head map for track starting'
                                              ' at offset'
                                              ' {:d}.'.format(trackstart))
            else:
                head     = head & track.HEAD_MASK
                head_map = [head]*secs

            # Read sectors
            for n, secnum in enumerate(sec_map):
                sec = sector.sector()
                del sec[:]
                sec.cyl_num = cyl_map[n]
                sec.head_num = head_map[n]
                sec.sec_num  = secnum
                
                sec.sector_type = \
                  util.readord(fh, 'Unexpected EOF while reading sector type'
                                   ' for sector {:d} of track starting at'
                                   ' offset {:d}.'.format(secnum, trackstart))

                if sec.sector_type not in sector.SEC_REC_ALL_TYPES:
                    offset = fh.tell()-1
                    fh.close()
                    raise ValueError('Invalid sector type 0x{:02x} at'
                                     ' file offset'
                                     ' {:d}.'.format(sec.sector_type, offset))

                if sec.sector_type in sector.SEC_REC_NORM_TYPES:
                    # Read complete sector
                    secdata = \
                      util.readbytearray(fh, trk.sector_size,
                                         'Unexpected EOF while reading data'
                                         ' for sector {:d} of track starting'
                                         ' at offset'
                                         ' {:d}.'.format(secnum, trackstart))
                    sec.extend(secdata)
                elif sec.sector_type in sector.SEC_REC_COMP_TYPES:
                    # Read single byte and fill sector with it
                    secdata = \
                      util.readord(fh,
                                   'Unexpected EOF while reading compressed'
                                   ' data for sector {:d} of track starting'
                                   ' at offset'
                                   ' {:d}.'.format(secnum, trackstart))
                    sec.extend([secdata]*trk.sector_size)

                # Normalize the sector type. We will re-check for
                # compressibility at save time.
                sec.sector_type = sector.SEC_REC_COMPRESS_OFF[sec.sector_type]

                # Add sector to track
                trk.append(sec)
                
            # Create new cylinder if necessary
            if cyl >= len(self):
                self.extend([None]*((cyl+1) - len(self)))
            if self[cyl] is None:
                self[cyl] = cylinder.cylinder()

            # Add track to cylinder
            if head >= len(self[cyl]):
                self[cyl].extend([None]*((head+1) - len(self[cyl])))
            if self[cyl][head] is not None:
                fh.close()
                raise RuntimeError('Duplicate track {:d}.{:d} found'
                                   ' in image file at offset {:d}.'
                                   .format(cyl, head, trackstart))
            self[cyl][head] = trk

        # Done!
        fh.close()


    def save(self, filename):
        """Write disk image to IMD file, overwriting existing file if any."""

        # Open file
        fh = open(filename, 'wb')

        # If magic number is missing, add to comment and issue warning.
        if (len(self.comment) < 3) or (self.comment[0:3] != 'IMD'):
            warnings.warn('Adding IMD magic number.', Warning)
            self.comment = _magic + '\n' + self.comment

        # Write comment
        buf = self.comment.replace('\n', '\x0D\x0A').encode()
        fh.write(buf)
        fh.write(b'\x0D\x0A\x1A')

        # Write tracks
        for cyl in self:
            if cyl is not None:
                for trk in cyl:
                    if trk is not None:
                        # Will we need cylinder and head maps?
                        needs_cyl_map  = trk.needs_cyl_map()
                        needs_head_map = trk.needs_head_map()

                        # Mode, cyl number, head number w/ flags, sector
                        # count, sector size
                        fh.write(bytearray([trk.mode]))
                        fh.write(bytearray([trk.cyl_num]))
                        hd = trk.head_num
                        if needs_cyl_map:
                            hd = hd | track.SEC_CYL_MAP_FLAG
                        if needs_head_map:
                            hd = hd | track.SEC_HEAD_MAP_FLAG
                        fh.write(bytearray([hd]))
                        fh.write(bytearray([len(trk)]))
                        fh.write(bytearray([track.SEC_CODE_FROM_SIZE[
                                            trk.sector_size]]))

                        # Sector number map
                        for sec in trk:
                            fh.write(bytearray([sec.sec_num]))

                        # Cylinder map (optional)
                        if needs_cyl_map:
                            for sec in trk:
                                fh.write(bytearray([sec.cyl_num]))

                        # Head map (optional)
                        if needs_head_map:
                            for sec in trk:
                                fh.write(bytearray([sec.head_num]))

                        # Sector data
                        for sec in trk:
                            t = sec.sector_type
                            if sec.is_compressible():
                                t = sector.SEC_REC_COMPRESS_ON[t]
                            else:
                                t = sector.SEC_REC_COMPRESS_OFF[t]

                            fh.write(bytearray([t]))

                            if t in sector.SEC_REC_NORM_TYPES:
                                # Save full sector data
                                fh.write(sec)
                            elif t in sector.SEC_REC_COMP_TYPES:
                                # Save single byte
                                fh.write(bytearray([sec[0]]))

        # Done!
        fh.close()




    def details(self):
        """Return detailed description of disk contents."""


        desc = 'IMD file comment:\n'
        desc = desc + util.indent(self.comment + '\n')
        desc = desc + 'Track details with c.h.s lists (c=compressible,' \
                      ' *=DDAM, !=error):\n'

        trkdesc = ''

        for cyl_num,cyl in enumerate(self):
            if cyl is not None:
                for head_num,trk in enumerate(cyl):
                    if trk is not None:
                        trkdesc = trkdesc \
                            + 'Track {:2d}.{:d}:\n'.format(cyl_num,head_num)
                        trkdesc = trkdesc + util.indent(trk.details())

        desc = desc + util.indent(trkdesc)

        return desc


    def summary(self):
        """Return brief summary of disk contents."""

        modes     = []
        cylinders = []
        heads     = []
        sectors   = []
        sizes     = []

        for cyl_num, cyl in enumerate(self):
            if cyl is not None:
                for head_num, trk in enumerate(cyl):
                    if trk is not None:
                        modes.append(track.MODE_NAMES[trk.mode])
                        cylinders.append(cyl_num)
                        heads.append(head_num)
                        sectors.append(len(trk))
                        sizes.append(trk.sector_size)

        modes     = list(set(modes))
        cylinders = list(set(cylinders))
        heads     = list(set(heads))
        sectors   = list(set(sectors))
        sizes     = list(set(sizes))

        desc = '{:d} cylinders ({:d}...{:d})'.format(len(cylinders),
                                                     min(cylinders), 
                                                     max(cylinders))

        desc = desc + ', {:d} heads'.format(len(heads))

        if len(sectors) == 1:
            desc = desc + ', {:d} sectors'.format(sectors[0])
        else:
            desc = desc + ', {:s} sectors'.format(str(sorted(sectors)))

        if len(sizes) == 1:
            desc = desc + ' of {:d} bytes'.format(sizes[0])
        else:
            desc = desc + ' of {:s} bytes'.format(str(sorted(sizes)))

        if len(modes) == 1:
            desc = desc + ', {:s}'.format(modes[0])
        else:
            desc = desc + ', {:s}'.format(str(sorted(modes)))

        desc = self.comment + '\n' + util.indent(desc)

        return desc


    def interleave(self, skip=0):
        """Interleave sectors of all tracks by specified skip factor.

        Set skip factor to 0 to remove interleave."""

        for cyl in self:
            if cyl is not None:
                for trk in cyl:
                    if trk is not None:
                        trk.interleave(skip)


    def skew(self, sec_num=0):
        """Change track skew.

        Rearranges the track such that the sector with the specified
        number occurs after the index pulse. Sector order is preserved.
        If sector with matching number is not found, track will be left
        unmodified. If more than one sector has the same sector number
        assigned, the first one found will be used."""

        for cyl in self:
            if cyl is not None:
                for trk in cyl:
                    if trk is not None:
                        trk.skew(sec_num)


    def find_sector(self, cyl, head, secnum):
        """Return sector w/ specified cylinder, head and sector numbers.

        Sector number is logical number, not physical number.
        Returns None if sector cannot be found."""

        if cyl < len(self):
            if self[cyl] is not None:
                if head < len(self[cyl]):
                    if self[cyl][head] is not None:
                        return self[cyl][head].find_sector(secnum)
        return None


    def dump_sector(self, cyl, head, secnum):
        """Return hex dump of specified sector.

        Provide physical cylinder/head numbers, and logical sector number."""
        
        sec = self.find_sector(cyl,head,secnum)
        t = sec.sector_type
        if sec.is_compressible():
            t = sector.SEC_REC_COMPRESS_ON[t]
        else:
            t = sector.SEC_REC_COMPRESS_OFF[t]
        buf = '{:d}.{:d}.{:d}: {:s}\n'.format(cyl, head, secnum,
                                              sector.SEC_REC_TYPE_DESC[t])
        buf = buf + util.indent(sec.hexdump())
        return buf


    def dump_all(self):
        """Return hex dump of all sectors, in physical order."""

        buf = ''
        for cyl in self:
            if cyl is not None:
                for trk in cyl:
                    if trk is not None:
                        for sec in trk:
                            buf = buf + self.dump_sector(trk.cyl_num,
                                                         trk.head_num,
                                                         sec.sec_num) + '\n'
        return buf
        
