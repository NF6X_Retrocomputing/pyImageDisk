#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016-2020 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Support for creating and manipulating ImageDisk (.IMD) files."""

__all__       = ['disk', 'cylinder', 'track', 'sector', 'util']
__version__   = '1.3'
__copyright__ = 'Copyright (C) 2016-2020 Mark J. Blair, released under GPLv3'
__pkg_url__   = 'https://gitlab.com/NF6X_Retrocomputing/pyImageDisk'
__dl_url__    = 'https://gitlab.com/NF6X_Retrocomputing/pyImageDisk'

from .disk import *
from .cylinder import *
from .track import *
from .sector import *



