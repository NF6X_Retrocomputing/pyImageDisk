#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""This package defines the template for filesystem objects."""

import imd
import collections

# Filesystem dictionary entries will be instances of the fsdesc named tuple:
#   desc:       Description of filesystem
#   classname:  Class implementing filesystem
#   params:     Tuple of parameters to pass to class initializer
fsdesc = collections.namedtuple('fsdesc', 'desc classname params')


def filesystem(disk):
    """Filesystem objects map a filesystem onto the disk class.

    This is a template for function classes which will implement actual
    filesystems.
    """

    params = None

    def __init__(self,
                 orig     = None,
                 filename = None,
                 params   = None):
        """Creates a filesystem.

        If orig is another instance of this class, then copy it.

        Else if orig is an instance of the disk class, copy the sector
          data from it and try to infer the appropriate parameters from
          the image.

        Else if filename is not None, load the sector data from the
          specified file, and try to infer the appropriate parameters
          from the image.

        Otherwise, create an empty, formatted filesystem with speficied
          or default parameters.

        Pass a class-specific tuple of parameters with the params
        argument to select a specific version of a filesystem. Argument
        is ignored if filesystem does not offer any variations. Each
        class should set a reasonable default value for this argument.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def examine(self):
        """Examine sector contents to infer parameters.
        Set parameters if valid, or raise exception if not valid.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('This is a stub for future development.')


    def init(self,
             params=None):
        """Replace contents with image of blank formatted disk.

        Pass a class-specific tuple of parameters with the params
        argument to select a specific version of a filesystem. Argument
        is ignored if filesystem does not offer any variations. Each
        class should set a reasonable default value for this argument.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def files(self):
        """Return list of names of files on the disk.

        If filesystem supports subdirectories, then listing will be
        recursive, with paths relative to root of filesystem. Empty
        directories are not included.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def dirs(self):
        """Return list of names of directories on the disk (if applicable).

        List includes all directories, with paths relative to root of
        filesystem. If filesystem does not support subdirectories, then
        will return an empty list.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def listing(self):
        """Return a printable listing of the disk contents.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def addfile(self, filename, data):
        """Add a new file to the filesystem.

        If filesystem supports subdirectories, directories in filesystem
        path will be created if they do not already exist.

        data argument should be a bytearray containing desired file
        contents. If filesystem requires fixed block lengths, then data
        may be padded as needed.

        Filesystem-specific classes may include additional arguments for
        permissions, timestamps, etc. as appropriate, with reasonable
        default values.
        
        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def delfile(self, filename):
        """Delete the specified file from the filesystem.
        
        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def readfile(self, filename):
        """Return contents of specified file on filesystem.

        Returns a bytearray.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def getbootblocks(self):
        """Returns contents of filesystem boot blocks.

        Returns a bytearray if filesystem supports bootblocks, or None
        if not supported.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def setbootblocks(self, data):
        """Sets fiesystem bootblocks to provided data.

        Data should be a bytearray, and will be padded or truncated
        as needed.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def mkdir(self, dirname):
        """Create subdirectory with specified path.

        Raises exception if filesystem does not support subdirectories.

        Recursively creates entire path as necessary.

        Filesystem-specific classes may include additional arguments for
        permissions, timestamps, etc. as appropriate, with reasonable
        default values.
        
        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')


    def deldir(self, dirname):
        """Deletes the specified directory from the filesystem.

        This virtual function must be defined in filesystem-specific
        classes.
        """
        raise exceptions.NotImplementedError('Virtual function must be defined by inheritor.')

    
