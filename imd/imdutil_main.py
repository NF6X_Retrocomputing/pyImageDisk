#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Main code for imdutil.py script."""

import sys
import argparse
import textwrap
import imd
from imd import util

def imdutil_main():

    """main() function for imdutil.py script.

    imdutil.py is simply a stub that calls this function.
    Placing the code in here makes unit tests and code
    coverage measurement easier.

    Returns code that should be passed to exit() by caller.

    Since this expects to be treated like a main() function
    in a script, it looks for command line arguments in sys.argv
    like any command line script would."""


    # This is the in-memory buffer of the disk image
    diskbuf = imd.disk()

    # Set up the command-line argument parser
    parser = argparse.ArgumentParser(
        prog='imdutil',
        description=textwrap.dedent("""\
        ImageDisk (.IMD) utility version {:s}
          {:s}
          {:s}

        Arguments are processed in the order encountered, with
        cumulative effects upon the disk image buffer. The disk image
        buffer is discarded at program exit if not saved by final
        argument. Arguments may be abbreviated.

        The detailed description of image contents lists the sectors of
        each track in physical order, starting after the index pulse, in
        cylinder.head.sector format. Cylinder and head numbers are
        logical numbers, and may not match physical cylinder and head.
        Where appropriate, sector numbers are flagged with extra
        characters:

          !: Error while reading sector (no sector data is present)
          *: Sector with DDAM (deleted data address mark)
          c: Sector is compressible (contains single repeated byte)
        """.format(imd.__version__, imd.__copyright__, imd.__pkg_url__)),
        epilog=textwrap.dedent("""\
        Example:
          imdutil --load mydisk.imd --summary"""),
        add_help=False,
        formatter_class=argparse.RawDescriptionHelpFormatter)


    # Add help arguments manually instead of using add_help, because
    # default help argument handler calls sys.exit() which messes up
    # unit tests. Also, we can format the help text consistently with
    # the help text of other arguments.
    parser.add_argument('-h', '--help',  action=util.gather_args, nargs=0,
                        help="""Print help and exit.""")

    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {:s}'.format(imd.__version__),
                        help='Print version and exit.')

    parser.add_argument('-l', '--load', action=util.gather_args, nargs=1,
                        metavar='FILENAME',
                        help="""Load disk image buffer from file, replacing
                        previous buffer contents.""")

    parser.add_argument('-s', '--save', action=util.gather_args, nargs=1,
                        metavar='FILENAME',
                        help="""Save disk image buffer to file.""")

    parser.add_argument('-S', '--summary', action=util.gather_args, nargs=0,
                        help="""Print summary of image contents.""")

    parser.add_argument('-D', '--details', action=util.gather_args, nargs=0,
                        help="""Print detailed description of image contents.""")

    parser.add_argument('-c', '--comment', action=util.gather_args, nargs=1,
                        metavar='COMMENT',
                        help="""Set IMD file comment.""")

    parser.add_argument('-i', '--interleave', action=util.gather_args, nargs=1,
                        metavar='SKIP',
                        help="""Interleave sectors with specified skip factor.
                        A skip factor of 0 removes interleave.""")

    parser.add_argument('-k', '--skew', action=util.gather_args, nargs=1,
                        metavar='SECTOR_NUM',
                        help="""Rotate sector order to place specified sector
                        number after index pulse.""")

    parser.add_argument('-u', '--dumpsector', action=util.gather_args, nargs=1,
                        metavar='C.H.S',
                        help="""Print hexadecimal dump of sector. Specify the
                        sector in cylinder.head.sector format. Use physical
                        cylinder and head numbers, and logical sector number.""")

    parser.add_argument('-U', '--dumpall', action=util.gather_args, nargs=0,
                        help="""Print hexadecimal dump of all sectors,
                        in physical order.""")



    # Parse the command-line arguments. Need to create empty
    # arg_sequence in case no command-line arguments were included.
    args = parser.parse_args()
    if not 'arg_sequence' in args:
        setattr(args, 'arg_sequence', [])


    # Execute each command-line argument in the order encountered.
    for arg in args.arg_sequence:
        cmd = arg[0]
        opt = arg[1]

        if cmd == 'help':
            parser.print_help()
            return(0)

        elif cmd == 'load':
            diskbuf.load(opt[0])

        elif cmd == 'save':
            diskbuf.save(opt[0])

        elif cmd == 'summary':
            print(diskbuf.summary())

        elif cmd == 'details':
            print(diskbuf.details())

        elif cmd == 'comment':
            diskbuf.comment = opt[0]

        elif cmd == 'interleave':
            diskbuf.interleave(int(opt[0]))

        elif cmd == 'skew':
            diskbuf.skew(int(opt[0]))

        elif cmd == 'dumpsector':
            cyl,head,sec = list(map(int,opt[0].split('.')))
            print(diskbuf.dump_sector(cyl, head, sec))

        elif cmd == 'dumpall':
            print(diskbuf.dump_all())

    return(0)
