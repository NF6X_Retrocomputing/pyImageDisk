#!/usr/bin/env python
#
########################################################################
# Copyright (C) 2016 Mark J. Blair, NF6X
#
# This file is part of pyImageDisk.
#
#  pyImageDisk is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyImageDisk is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Full unit test suite for pyImageDisk."""

import unittest
from imd import test_imd
from imd import test_imdutil_main

class test_all(unittest.TestSuite):
    """Test suite containing all pyImageDisk unit tests."""

    def __init__(self):
        super(test_all, self).__init__()
        self.addTest(test_imd.suite())
        self.addTest(test_imdutil_main.suite())


def suite():
    return test_all()

# Call suite() on import so the function gets some minimal coverage
# during regression runs.
suite()
