# Change Log for pyImageDisk

## v1.3

* Add -v/--version flag.
* Import cylinder class at top level.

## v1.2

* **Rename `imdutil.py` to `imdutil`**

* Check for IMD magic number when loading file. Issue warning if it is not present.

* Check for IMD magic number when saving file. Issue warning and add it if it is not present.

* Promote pyImageDisk from alpha to beta status.
