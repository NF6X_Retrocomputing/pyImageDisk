# pyImageDisk: Python support for manipulating ImageDisk files

## INTRODUCTION

    pyImageDisk provides Python support for creation and manipulation
    of ImageDisk (.IMD) files. ImageDisk is a DOS-based utility for
    archival and recreation of floppy disks in a wide variety of
    formats, and its archive file format is convenient for archival
    of many FM and MFM formats used by floppy-disk-based
    computers with common floppy disk controller chips.

    The primary utility script for image manipulation is imdutil.py.
    Invoke imdutil.py with the '-h' or '--help' option for
    instructions.

## SYSTEM REQUIREMENTS

    Any system with a compatible Python installation should be able to
    use pyImageDisk.
     

## INSTALLATION

    Use the setup.py script to install pyImageDisk. Here are some usage
    examples:

    Install in your user directory:
        ./setup.py install --user

    Install in the system default location:
        sudo ./setup.py install

    Install under /usr/local:
        sudo ./setup.py install --prefix /usr/local

    Run regression tests on the package:
        ./setup.py test


## AUTHOR

     Mark J. Blair, NF6X <nf6x@nf6x.net>

## COPYING

    Copyright (C) 2016-2019 Mark J. Blair, NF6X
        
    pyImageDisk is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    pyImageDisk is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License
    along with pyImageDisk.  If not, see <http://www.gnu.org/licenses/>.

